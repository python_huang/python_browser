from selenium import webdriver
import threading as thd

# 建立 driver
driver = webdriver.Chrome()
# 去 google
f = open('url.txt', 'r')
driver.get(f.read())
# 找到搜尋框
element = driver.find_element_by_name("q")
# 搜尋框輸入字
element.send_keys("cheese!")
# 提交
element.submit()


# 自動刷新設定
def refresh():
    driver.refresh()
    # 秒數設定
    thd.Timer(5, refresh).start()


# 刷新
refresh()

with open('test.html', 'wb') as f:
    f.write(driver.page_source.encode('utf-8'))
